# MA Inverted Pendulum Bucher

This repository contains the implementation of reinforcement algorithms for the control of an inverse pendulum. 


The written elaboration of the master thesis in overleaf can be found here: ["MA_Inverted_Pendulum"](https://www.overleaf.com/project/660d3d3e70dd1015b18015bf)


## TO - DO:

- [ ] Plot function der einzelnen GP modelle implementieren
- [x] Matlab Daten transferieren

- [ ] Organisation
    - [ ] ifas GitLab
    - [ ] Themenstellung
    - [ ] Anmeldung

- [ ] Einarbeitung LA
    - [ ] Hydraulischen Schaltplan HELAX verstehen
    - [ ] Herkömmliche Regelung verstehen
    - [ ] Ansteuerung/Worklflow verstehen

- [ ] Pendel regeln
    - [x] PILCO in MATLAB implementieren
    - [ ] PILCO in Simulink einbetten
    - [ ] Rechenzeit berechnen
    - [ ] Hyperparameter anpassen
        - [ ] Cost function anpassen
        - [ ] Optimizer anpassen ?

- [ ] Schriftliche Ausarbeitung
    - [ ] SdT
    - [ ] Methodik und Vorgehensweise
    - [ ] ...

-------------------------------------------------------------------------

- [ ] PILCO "verbessern"
    - [ ] Deep PILCO
    - [ ] safe PILCO
    - [ ] sparse MGPR
    - [ ] physics informed
    - [ ] RL + MPC
    - [ ] Hybrid RL

- [ ] neue offline RL Modelle implementieren
    - [ ] MOReL
    - [ ] MOPO
    - [ ] ...


## Zeitplan

![Zeitplan](assets/images/Zeitplan.svg)