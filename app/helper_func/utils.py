"""This module stores utility functions"""
import time
import json
import numpy as np
import matplotlib.pyplot as plt
from gpflow import config
from gym import make
float_type = config.default_float()

def convert_arrays_to_lists(d):
    """converts np arrays to lists"""
    return [{key: value.tolist()} if isinstance(value, np.ndarray)
            else {key: value} for key, value in d.items()]

def convert_list_to_array(d):
    """converts lists to np arrays"""
    transformed_data = {}
    if isinstance(d, list):
        for param_list in d:
            for param_key, param_value in param_list.items():
                if isinstance(param_value, list):
                    transformed_data[param_key] = np.array(param_value)
                else:
                    transformed_data[param_key] = param_value
    else:
        for key, values in d.items():
            gp_params = {}
            for param_dict in values:
                for param_key, param_value in param_dict.items():
                    if isinstance(param_value, list):
                        gp_params[param_key] = np.array(param_value)
                    else:
                        gp_params[param_key] = param_value
            transformed_data[key] = gp_params

    return transformed_data

def save_model(name, data, gpmodel, controller, dim):
    """saving PILCO"""
    gpmodel_dict = {}
    controller_dict = [arr.tolist() for arr in controller]
    for i in range(dim):
        gpmodel_dict["GP" + str(i)] = convert_arrays_to_lists(gpmodel["GP"+ str(i)])

    model_dict = {
        "X_model": data[0].tolist(),  
        "Y_model": data[1].tolist(),
        "model_parameter": gpmodel_dict,
        "controller_parameter": controller_dict
    }

    with open(f'app/simulation/saved_models/{name}.json', "w", encoding = "utf8") as f:
        json.dump(model_dict, f)

def load_model(filename):
    """loading PILCO"""
    with open(f'app/simulation/saved_models/{filename}.json', "r", encoding = "utf8") as f:
        model_dict = json.load(f)

    x = np.asarray(model_dict["X_model"], dtype=np.float64)
    y = np.asarray(model_dict["Y_model"], dtype=np.float64)
    model_param = convert_list_to_array(model_dict["model_parameter"])
    controller_param = [np.array(list) for list in model_dict["controller_parameter"]]

    return x, y, model_param, controller_param


def plot(t, X_new, state_dim, m_init, s_init, pilco, rollouts, show=False, save=False):
    """plots predictions"""
    m_p = np.zeros((t, state_dim))
    s_p = np.zeros((t, state_dim, state_dim))

    m_p[0,:] = m_init
    s_p[0, :, :] = s_init

    for h in range(1, t):
        m_p[h,:], s_p[h,:,:] = pilco.propagate(m_p[h-1, None, :], s_p[h-1,:,:])

    _, axs = plt.subplots(state_dim, 1, figsize=(10, 8), sharex=True)

    for i in range(state_dim):
        axs[i].plot(range(t-1), m_p[0:t-1, i], label='mean_prediction')
        axs[i].plot(range(t-1), X_new[1:t, i], linestyle='--', label='X_new')
        axs[i].fill_between(range(t-1),
                        m_p[0:t-1, i] - 2*np.sqrt(s_p[0:t-1, i, i]),
                        m_p[0:t-1, i] + 2*np.sqrt(s_p[0:t-1, i, i]), alpha=0.2)

        axs[i].set_xlabel('Time Step')
        axs[i].set_ylabel('Value')

    axs[-1].legend()
    plt.suptitle(f"ITERATION {rollouts}")

    if save is True:
        plt.savefig(f'app/simulation/saved_models/{rollouts}.png')

    if show is True:
        plt.show()


def rollout(env, pilco, timesteps, verbose=True, random=False, SUBS=1, render=False):
    """interaction of the agent with the environment. Collects new data"""
    X = []
    Y = []
    x = env.reset()
    ep_return_full = 0
    ep_return_sampled = 0
    for _ in range(timesteps):
        if render:
            env.render()
        u = policy(env, pilco, x, random)
        for _ in range(SUBS):
            x_new, r, done, _ = env.step(u)
            ep_return_full += r
            if done:
                break
            if render:
                env.render()
        if verbose:
            print("Action: ", u)
            print("State : ", x_new)
            print("Return so far: ", ep_return_full)
        X.append(np.hstack((x, u)))
        Y.append(x_new - x)
        ep_return_sampled += r
        x = x_new
        if done:
            break
    return np.stack(X), np.stack(Y), ep_return_sampled, ep_return_full

def control(env, pilco, timesteps, verbose=True, random=False, SUBS=1, render=False):
    """interaction of the agent with the environment"""
    X = []
    Y = []
    x = env.reset()
    ep_return_full = 0
    ep_return_sampled = 0
    for timestep in range(1, timesteps+1):
        if render:
            env.render()
        if timestep % 50 == 0:
            x_new, r, done, _ = env.random_action()
        else:
            start = time.time()
            u = policy(env, pilco, x, random)
            end = time.time()
            for _ in range(SUBS):
                x_new, r, done, _ = env.step(u)
                ep_return_full += r
                if done:
                    break
                if render:
                    env.render()
            print("Inference done in: %.6f seconds."
                    % (end - start))               
        if verbose:
            print("Timestep:", timestep)
            if timestep % 50 == 0:
                print("Action: Random")
            else:
                print("Action:", u)
            print("State:", x_new)
            print("Return so far:", ep_return_full)
        X.append(np.hstack((x, u)))
        Y.append(x_new - x)
        ep_return_sampled += r
        x = x_new
        if done:
            break
    return np.stack(X), np.stack(Y), ep_return_sampled, ep_return_full

def policy(env, pilco, x, random):
    """..."""
    if random:
        return env.action_space.sample()

    return pilco.compute_action(x[None, :])[0, :]

class NormalisedEnv():
    """..."""
    def __init__(self, env_id, m, std):
        self.env = make(env_id).env
        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space
        self.m = m
        self.std = std

    def state_trans(self, x):
        """..."""
        return np.divide(x-self.m, self.std)

    def step(self, action):
        """..."""
        ob, r, done, _ = self.env.step(action)
        return self.state_trans(ob), r, done, {}

    def reset(self):
        """..."""
        ob =  self.env.reset()
        return self.state_trans(ob)

    def render(self):
        """..."""
        self.env.render()
