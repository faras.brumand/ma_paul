"..."
import os
import sys

parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
# Add the parent directory to the Python path if it's not already there
if parent_dir not in sys.path:
    sys.path.append(parent_dir)

from machine_learning.pilco.models import PILCO
from machine_learning.pilco.controllers import RbfController
from machine_learning.pilco.rewards import ExponentialReward
from helper_func.utils import control, load_model
from gpflow import set_trainable

import numpy as np
import pandas as pd
import gpflow
import tensorflow as tf
import gym

np.random.seed(0)

# Set the global default float type to float64
tf.keras.backend.set_floatx('float64')


class MyPendulum():
    """Class for changed gym environment"""

    def __init__(self):
        self.env = gym.make('Pendulum-v1').env  #'CartPole-v1'
        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space

    def step(self, action):
        return self.env.step(action)

    def reset(self):
        high = np.array([np.pi, 1])
        self.env.state = np.random.uniform(low=-high, high=high)
        self.env.state = np.random.uniform(low=0, high=0.01*high)
        self.env.state[0] += -np.pi
        self.env.last_u = None
        return self.env._get_obs()

    def random_action(self):
        # Sample a random action from the action space
        random_action = self.action_space.sample()
        action = [2]
        # Apply the random action to the environment
        return self.step(action)

    def render(self):
        self.env.render()


if __name__ == '__main__':
    SUBS = 3
    BF = 30
    MAXITER = 50
    MAX_ACTION = 2.0
    TARGET = np.array([1.0, 0.0, 0.0])
    WEIGHTS = np.diag([2.0, 2.0, 0.3])
    M_INIT = np.reshape([-1.0, 0, 0.0], (1,3))
    S_INIT = np.diag([0.01, 0.05, 0.01])
    T = 40
    T_SIM = T
    J = 4
    N = 1
    RESTARTS = 1

    env = MyPendulum()

    # load models
    X, Y, model_param, controller_param = load_model("04_22-17_19")

    state_dim = Y.shape[1]
    control_dim = X.shape[1] - state_dim

    # initialize
    controller = RbfController(state_dim=state_dim, control_dim=control_dim,
                               num_basis_functions=BF, max_action=MAX_ACTION)

    R = ExponentialReward(state_dim=state_dim, t=TARGET, W=WEIGHTS)

    pilco = PILCO((X, Y), controller=controller, horizon=T,
                  reward=R, m_init=M_INIT, S_init=S_INIT)

    # for numerical stability, we can set the likelihood variance parameters of the GP models
    for model in pilco.mgpr.models:
        model.likelihood.variance.assign(0.001)
        set_trainable(model.likelihood.variance, False)

    pilco.optimize_models(maxiter=MAXITER, restarts=RESTARTS)

    # assign parameters
    for i, model in enumerate(pilco.mgpr.models):
        gpflow.utilities.multiple_assign(model, model_param[f'GP{i}'])

    pilco.mgpr.set_data((X, Y))

    #gpflow.utilities.multiple_assign(pilco.controller, controller_param)
    pilco.initialize_policy(controller_param)

    lengthscales = {}; variances = {}; noises = {}
    i = 0
    for model in pilco.mgpr.models:
        lengthscales['GP' + str(i)] = model.kernel.lengthscales.numpy()
        variances['GP' + str(i)] = np.array([model.kernel.variance.numpy()])
        noises['GP' + str(i)] = np.array([model.likelihood.variance.numpy()])
        i += 1
    print('-----Loaded GP models with parameters:------')
    pd.set_option('precision', 3)
    print('---Lengthscales---')
    print(pd.DataFrame(data=lengthscales))
    print('---Variances---')
    print(pd.DataFrame(data=variances))
    print('---Noises---')
    print(pd.DataFrame(data=noises))

    r_new = np.zeros((T, 1))
    for i in range(1):
        print("**** In control loop ****")
        X_new, Y_new, _, _ = control(env, pilco, timesteps=150,
                                    verbose=True, SUBS=SUBS, render=True)

        # Since we had decide on the various parameters of the reward function
        # we might want to verify that it behaves as expected by inspection
        for i in range(len(X_new)):
                r_new[:, 0] = R.compute_reward(X_new[i, None, :-1],
                                               0.001 * np.eye(state_dim))[0]
        total_r = sum(r_new)
        _, _, r = pilco.predict(X_new[0, None, :-1], 0.001 * S_INIT, T)
        print("Total ", total_r, " Predicted: ", r)
