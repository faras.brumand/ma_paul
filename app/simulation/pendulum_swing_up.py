import os
import sys
import time

parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
# Add the parent directory to the Python path if it's not already there
if parent_dir not in sys.path:
    sys.path.append(parent_dir)

from machine_learning.pilco.models import PILCO
from machine_learning.pilco.controllers import RbfController
from machine_learning.pilco.rewards import ExponentialReward
from helper_func.utils import rollout, save_model
from gpflow import set_trainable

import tensorflow as tf
import numpy as np
import gym

# Set the global default float type to float64
tf.keras.backend.set_floatx('float64')

np.random.seed(0)

# NEEDS a different initialisation than the one in gym (change the reset() method),
# to (m_init, S_init), modifying the gym env

# Introduces subsampling with the parameter SUBS and modified rollout function
# Introduces priors for better conditioning of the GP model
# Uses restarts

class MyPendulum():
    """Class for changed gym environment"""

    def __init__(self):
        self.env = gym.make('Pendulum-v1').env    #'Pendulum-v1'    'CartPole-v1'
        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space

    def step(self, action):
        return self.env.step(action)

    def reset(self):
        high = np.array([np.pi, 1])
        self.env.state = np.random.uniform(low=-high, high=high)
        self.env.state = np.random.uniform(low=0, high=0.01*high) # only difference
        self.env.state[0] += -np.pi
        self.env.last_u = None
        return self.env._get_obs()

    def random_action(self):
        # Sample a random action from the action space
        random_action = self.action_space.sample()
        # Apply the random action to the environment
        return self.step(random_action)

    def render(self):
        self.env.render()

if __name__=='__main__':
    SUBS=3
    BF = 30
    MAXITER=50
    MAX_ACTION=2.0
    TARGET = np.array([1.0, 0.0, 0.0])
    WEIGHTS = np.diag([2.0, 2.0, 0.3])
    M_INIT = np.reshape([-1.0, 0, 0.0], (1,3))
    S_INIT = np.diag([0.01, 0.05, 0.01])
    T = 40
    T_SIM = T
    J = 4
    N = 1
    RESTARTS = 1

    env = MyPendulum()

    # Initial random rollouts to generate a dataset
    X, Y, _, _ = rollout(env, None, timesteps=T, random=True, SUBS=SUBS, render=True)
    X = X.astype(np.float64)
    Y = Y.astype(np.float64)
    for i in range(1,J):
        X_, Y_, _, _ = rollout(env, None, timesteps=T, random=True,
                                SUBS=SUBS, verbose=True, render=True)
        X_ = X_.astype(np.float64)
        Y_ = Y_.astype(np.float64)
        X = np.vstack((X, X_))
        Y = np.vstack((Y, Y_))

    state_dim = Y.shape[1]
    control_dim = X.shape[1] - state_dim

    print(X.shape)
    print(Y.shape)

    print(state_dim)
    print(control_dim)


    controller = RbfController(state_dim=state_dim, control_dim=control_dim,
                                num_basis_functions=BF, max_action=MAX_ACTION)

    R = ExponentialReward(state_dim=state_dim, t=TARGET, W=WEIGHTS)

    pilco = PILCO((X, Y), controller=controller, horizon=T, reward=R, m_init=M_INIT, S_init=S_INIT)

    # for numerical stability, we can set the likelihood variance parameters of the GP models
    for model in pilco.mgpr.models:
        model.likelihood.variance.assign(0.001)
        set_trainable(model.likelihood.variance, False)

    r_new = np.zeros((T, 1))
    for rollouts in range(N):
        print("**** ITERATION no", rollouts, " ****")

        model_param = pilco.optimize_models(maxiter=MAXITER, restarts=RESTARTS)
        controller_param = pilco.optimize_policy(maxiter=MAXITER, restarts=RESTARTS)

        X_new, Y_new, _, _ = rollout(env, pilco, timesteps=T_SIM,
                                      verbose=True, SUBS=SUBS, render=True)

        # Since we had decide on the various parameters of the reward function
        # we might want to verify that it behaves as expected by inspection
        for i in range(len(X_new)):
                r_new[:, 0] = R.compute_reward(X_new[i,None,:-1], 0.001 * np.eye(state_dim))[0]
        total_r = sum(r_new)
        _, _, r = pilco.predict(X_new[0,None,:-1], 0.001 * S_INIT, T)
        print("Total ", total_r, " Predicted: ", r)

        # Update dataset
        X = np.vstack((X, X_new)); Y = np.vstack((Y, Y_new))
        pilco.mgpr.set_data((X, Y))

        #for model in pilco.mgpr.models:
            #plot_prediction(model.predict_y)

        #plot(T, X_new, state_dim, m_init, S_init, pilco, rollouts, show=False, save=True)

        if rollouts == N-1:
            save_model(time.strftime("%m_%d-%H_%M"), (X,Y), model_param,
                                        controller_param, state_dim)
