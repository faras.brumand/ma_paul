import os
import sys

import numpy as np
import gym
from gym import spaces

parent_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
# Add the parent directory to the Python path if it's not already there
if parent_dir not in sys.path:
    sys.path.append(parent_dir)


from machine_learning.pilco.models import PILCO
from machine_learning.pilco.controllers import RbfController, LinearController
from machine_learning.pilco.rewards import ExponentialReward
import tensorflow as tf


# Set the global default float type to float64
tf.keras.backend.set_floatx('float64')

from helper_func.utils import rollout, policy
from gpflow import set_trainable
np.random.seed(0)

# NEEDS a different initialisation than the one in gym (change the reset() method),
# to (m_init, S_init), modifying the gym env

# Introduces subsampling with the parameter SUBS and modified rollout function
# Introduces priors for better conditioning of the GP model
# Uses restarts


#env = gym.make('CartPole-v1') 

class MyCartPoleEnv(gym.Env):
    def __init__(self):
        self.env = gym.make('CartPole-v1')  
        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space

    def step(self, action):
        return self.env.step(self.convert_action(action))

    def reset(self):
        return self.env.reset()

    def render(self):
        self.env.render()

    def convert_action(self, action):
        if action < 0:
            return 0
        else:
            return 1

if __name__=='__main__':
    SUBS=3
    bf = 30
    maxiter=50
    max_action=2.0
    target = np.array([1.0, 0.0, 0.0, 0.0])
    weights = np.diag([2.0, 2.0, 0.3, 1.0])
    m_init = np.reshape([-1.0, 0, 0.0, 0.0], (1,4))
    S_init = np.diag([0.01, 0.05, 0.01, 0.01])
    T = 100
    T_sim = T
    J = 4
    N = 15
    restarts = 2

    #env.reset()
    env = MyCartPoleEnv()

    # Initial random rollouts to generate a dataset
    X, Y, _, _ = rollout(env, None, timesteps=T, random=True, SUBS=SUBS, render=True)
    X = X.astype(np.float64)  
    Y = Y.astype(np.float64)  
    for i in range(1,J):
        X_, Y_, _, _ = rollout(env, None, timesteps=T, random=True, SUBS=SUBS, verbose=True, render=True)
        X_ = X_.astype(np.float64)  
        Y_ = Y_.astype(np.float64) 
        X = np.vstack((X, X_))
        Y = np.vstack((Y, Y_))
        
    print(X.shape)
    print(Y.shape)
    state_dim = Y.shape[1]
    control_dim = X.shape[1] - state_dim

    print(state_dim)
    print(control_dim)

    controller = RbfController(state_dim=state_dim, control_dim=control_dim, num_basis_functions=bf, max_action=max_action)
    R = ExponentialReward(state_dim=state_dim, t=target, W=weights)

    pilco = PILCO((X, Y), controller=controller, horizon=T, reward=R, m_init=m_init, S_init=S_init)

    # for numerical stability, we can set the likelihood variance parameters of the GP models
    for model in pilco.mgpr.models:
        model.likelihood.variance.assign(0.001)
        set_trainable(model.likelihood.variance, False)

    r_new = np.zeros((T, 1))
    for rollouts in range(N):
        print("**** ITERATION no", rollouts, " ****")
        pilco.optimize_models(maxiter=maxiter, restarts=2)
        pilco.optimize_policy(maxiter=maxiter, restarts=2)

        X_new, Y_new, _, _ = rollout(env, pilco, timesteps=T_sim, verbose=True, SUBS=SUBS, render=True)

        # Since we had decide on the various parameters of the reward function
        # we might want to verify that it behaves as expected by inspection
        for i in range(len(X_new)):
                r_new[:, 0] = R.compute_reward(X_new[i,None,:-1], 0.001 * np.eye(state_dim))[0]
        total_r = sum(r_new)
        _, _, r = pilco.predict(X_new[0,None,:-1], 0.001 * S_init, T)
        print("Total ", total_r, " Predicted: ", r)

        # Update dataset
        X = np.vstack((X, X_new)); Y = np.vstack((Y, Y_new))
        pilco.mgpr.set_data((X, Y))
