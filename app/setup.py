import subprocess

def install_packages_from_requirements(requirements_file):
    with open(requirements_file, 'r') as f:
        for line in f:
            package = line.strip()
            if package and not package.startswith('#'):
                subprocess.run(['pip', 'install', package])

if __name__ == "__main__":
    requirements_file = 'requirements.txt'
    install_packages_from_requirements(requirements_file)